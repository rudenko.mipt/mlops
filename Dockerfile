FROM python:3.12-slim

WORKDIR /app

RUN apt-get -y update
RUN apt-get -y install git nano
RUN pip install poetry==1.8.2 
RUN poetry config virtualenvs.create false


COPY pyproject.toml poetry.lock ./
RUN poetry install 
#RUN poetry install --with dev
#RUN poetry run kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data


COPY mlops /app/mlops


# ENTRYPOINT ["python", "/app/mlops/example.py"]
# CMD ["/bin/bash"]

