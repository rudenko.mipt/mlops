import pandas as pd

# Загрузка данных
data = pd.read_csv(snakemake.input[0])

# Простая предобработка: заполнение пропущенных значений и кодирование категориальных признаков
data["Age"].fillna(data["Age"].median(), inplace=True)
data["Embarked"].fillna("S", inplace=True)
data = pd.get_dummies(data, columns=["Sex", "Embarked"])

# Сохранение обработанных данных
data.to_csv(snakemake.output[0], index=False)
