import pandas as pd
from joblib import dump
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

# Загрузка данных
data = pd.read_csv(snakemake.input[0])

# Подготовка данных для обучения
X = data.drop(["Survived", "Name", "Ticket", "Cabin"], axis=1)
y = data["Survived"]

# Разделение данных на тренировочные и тестовые
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)

# Обучение модели
model = RandomForestClassifier(n_estimators=100, random_state=42)
model.fit(X_train, y_train)

# Сохранение модели
dump(model, snakemake.output[0])
