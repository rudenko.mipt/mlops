## Getting Started

1. Fork repo
2. Clone your fork
3. Create a new branch for your feature
4. Make your changes and commit them
5. Push your changes to your fork
6. Submit a pull request

## Using Linters

We use ruff for linting Python code. To check your code execute "ruff check"/
